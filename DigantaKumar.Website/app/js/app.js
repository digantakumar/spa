'use strict';

var myApp = angular.module('myApp', ['ngResource', 'ngRoute']) //add a ref. to ngRoute module 
    .config(function ($routeProvider) { //config is run when the app is first bootstrap by angularjs

        $routeProvider.when('/Home',
        {
            templateUrl: 'templates/Home.html',
            controller: 'HomeController'
        });

        $routeProvider.when('/MyForm',
        {
            templateUrl: 'templates/MyForm.html',
            controller: 'EditMyFormController'
        });
        $routeProvider.otherwise({ redirectTo: '/Home' }); //default route
        //$locationProvider.html5Mode(false).hashPrefix('!');
    });

