'use strict';

myApp.controller('FormController',
    function MyFormController($scope, eventData) {
    	$scope.sortorder = 'name';
        $scope.event = eventData.getEvent();

        $scope.upVoteSession = function(session) {
            session.upVoteCount++;
        };


        $scope.downVoteSession = function(session) {
            session.upVoteCount--;
        };
    }
);
